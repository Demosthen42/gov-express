const express = require('express');
const app = express();
const cors = require('cors');
var compression = require('compression');

const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const dotenv = require('dotenv');
dotenv.config();

const path = require('path');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors())

const url = "mongodb+srv://"+process.env.MONGODB_USER+":"+process.env.MONGODB_PASSWORD+"@"+process.env.MONGODB_SERVER+"/"+process.env.MONGODB_DATABASE;
mongoose.connect(url, {useNewUrlParser: true, useFindAndModify: false, retryWrites: true});

const db = mongoose.connection;

app.use(compression());
app.use('/api/v1', require('./routes/api/v1'));
app.use(express.static('www'));
app.get('*', (req, res) =>{
    res.sendFile(path.join(__dirname, '/www/index.html'));
});


const PORT = process.env.PORT;
app.listen(PORT, () => console.log('Server started'));