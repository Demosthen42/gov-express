(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["zakrd-zakrd-module"],{

/***/ "./src/app/zakrd/zakrd.module.ts":
/*!***************************************!*\
  !*** ./src/app/zakrd/zakrd.module.ts ***!
  \***************************************/
/*! exports provided: ZakrdPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZakrdPageModule", function() { return ZakrdPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _zakrd_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./zakrd.page */ "./src/app/zakrd/zakrd.page.ts");







var routes = [
    {
        path: '',
        component: _zakrd_page__WEBPACK_IMPORTED_MODULE_6__["ZakrdPage"]
    }
];
var ZakrdPageModule = /** @class */ (function () {
    function ZakrdPageModule() {
    }
    ZakrdPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_zakrd_page__WEBPACK_IMPORTED_MODULE_6__["ZakrdPage"]]
        })
    ], ZakrdPageModule);
    return ZakrdPageModule;
}());



/***/ }),

/***/ "./src/app/zakrd/zakrd.page.html":
/*!***************************************!*\
  !*** ./src/app/zakrd/zakrd.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button (click)=\"onBack(nd)\" defaultHref=\"/zakinfo\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Информация о редакции</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list *ngIf=\"show\" lines=\"none\">\n    <ion-item lines=\"none\" text-wrap>\n      <ion-label>Редакция: {{zakText.rd_list[0].date}}</ion-label>\n    </ion-item>\n\n\n    <ion-text>\n\n      <pre>{{text}}</pre>\n\n    </ion-text>\n\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/zakrd/zakrd.page.scss":
/*!***************************************!*\
  !*** ./src/app/zakrd/zakrd.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3pha3JkL3pha3JkLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/zakrd/zakrd.page.ts":
/*!*************************************!*\
  !*** ./src/app/zakrd/zakrd.page.ts ***!
  \*************************************/
/*! exports provided: ZakrdPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZakrdPage", function() { return ZakrdPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_gov_base_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/gov-base.service */ "./src/app/services/gov-base.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var ZakrdPage = /** @class */ (function () {
    function ZakrdPage(api, loadingController, navCtrl, route) {
        var _this = this;
        this.api = api;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.route = route;
        this.show = false;
        this.route.queryParams.subscribe(function (params) {
            _this.nd = params.nd;
            _this.rd = params.rd;
        });
    }
    ZakrdPage.prototype.ionViewDidEnter = function () {
        this.getZak_text(this.nd, this.rd);
    };
    ZakrdPage.prototype.cleanView = function () {
        //this.text[0] = this.zakText.rd_list[0].text;
    };
    ZakrdPage.prototype.getZak_text = function (nd, rd) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'Загрузка текста редакции'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.api.getZakText(nd, rd)
                            .subscribe(function (res) {
                            _this.zakText = res;
                            _this.text = _this.zakText.rd_list[0].text;
                            _this.cleanView();
                            _this.show = true;
                            loading.dismiss();
                        }, function (err) {
                            console.log(err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ZakrdPage.prototype.onBack = function (nd) {
        var navigationExtras = {
            queryParams: {
                nd: nd
            }
        };
        this.navCtrl.navigateBack(['zakinfo'], navigationExtras);
    };
    ZakrdPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-zakrd',
            template: __webpack_require__(/*! ./zakrd.page.html */ "./src/app/zakrd/zakrd.page.html"),
            styles: [__webpack_require__(/*! ./zakrd.page.scss */ "./src/app/zakrd/zakrd.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_gov_base_service__WEBPACK_IMPORTED_MODULE_3__["GovBaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], ZakrdPage);
    return ZakrdPage;
}());



/***/ })

}]);
//# sourceMappingURL=zakrd-zakrd-module.js.map