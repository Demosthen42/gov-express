(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["zakinfo-zakinfo-module"],{

/***/ "./src/app/zakinfo/zakinfo.module.ts":
/*!*******************************************!*\
  !*** ./src/app/zakinfo/zakinfo.module.ts ***!
  \*******************************************/
/*! exports provided: ZakinfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZakinfoPageModule", function() { return ZakinfoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _zakinfo_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./zakinfo.page */ "./src/app/zakinfo/zakinfo.page.ts");







var routes = [
    {
        path: '',
        component: _zakinfo_page__WEBPACK_IMPORTED_MODULE_6__["ZakinfoPage"]
    }
];
var ZakinfoPageModule = /** @class */ (function () {
    function ZakinfoPageModule() {
    }
    ZakinfoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_zakinfo_page__WEBPACK_IMPORTED_MODULE_6__["ZakinfoPage"]]
        })
    ], ZakinfoPageModule);
    return ZakinfoPageModule;
}());



/***/ }),

/***/ "./src/app/zakinfo/zakinfo.page.html":
/*!*******************************************!*\
  !*** ./src/app/zakinfo/zakinfo.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/list\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Информация о законе</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list *ngIf=\"show\">\n    <ion-item lines=\"none\">\n      <ion-label text-wrap>{{zak.name}}</ion-label>\n    </ion-item>\n\n\n    <ion-item button (click)=\"toggle2 = !toggle2\">\n      <ion-label>Сравнить редакции</ion-label>\n    </ion-item>\n    <ion-list [hidden]=\"!toggle2\" lines=\"none\">\n      <ion-item>\n        <ion-label>Первый</ion-label>\n        <ion-select [interfaceOptions]=\"customOptions\" interface=\"action-sheet\" placeholder=\"Первый\" [(ngModel)]=\"f\">\n          <ion-select-option *ngFor=\"let red of zak.rd_list\" [value]=\"red.rd\">{{red.date}}</ion-select-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label>Второй</ion-label>\n        <ion-select [interfaceOptions]=\"customOptions\" interface=\"action-sheet\" placeholder=\"Второй\" [(ngModel)]=\"s\">\n          <ion-select-option *ngFor=\"let red of zak.rd_list\" [value]=\"red.rd\">{{red.date}}</ion-select-option>\n        </ion-select>\n      </ion-item>\n      <ion-button (click)=\"onDiff()\">\n        Сравнить\n      </ion-button>\n    </ion-list>\n\n    <ion-item button (click)=\"onConnections(zak.nd)\" lines=\"all\">\n      <ion-label>Список связей</ion-label>\n    </ion-item>\n\n    <ion-item button (click)=\"toggle = !toggle\" lines=\"all\">\n      <ion-label>У закона {{zak.rds}} редакций:</ion-label>\n    </ion-item>\n\n\n    <ion-list [hidden]=\"!toggle\" >\n      <ion-item (click)=\"onClick(zak.nd ,item.rd)\" *ngFor=\"let item of zak.rd_list\" lines=\"all\">\n        <ion-label>{{item.date}}</ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-list>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/zakinfo/zakinfo.page.scss":
/*!*******************************************!*\
  !*** ./src/app/zakinfo/zakinfo.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3pha2luZm8vemFraW5mby5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/zakinfo/zakinfo.page.ts":
/*!*****************************************!*\
  !*** ./src/app/zakinfo/zakinfo.page.ts ***!
  \*****************************************/
/*! exports provided: ZakinfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZakinfoPage", function() { return ZakinfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_gov_base_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/gov-base.service */ "./src/app/services/gov-base.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var ZakinfoPage = /** @class */ (function () {
    function ZakinfoPage(api, loadingController, navCtrl, route) {
        var _this = this;
        this.api = api;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.route = route;
        this.show = false;
        if (this.show == false) {
            this.route.queryParams.subscribe(function (params) {
                _this.nd = params.nd;
            });
        }
    }
    ZakinfoPage.prototype.ionViewDidEnter = function () {
        if (this.show == false) {
            this.getZak(this.nd);
        }
    };
    //Загрузка и информации о законе
    ZakinfoPage.prototype.getZak = function (nd) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'Загрузка информации о законе'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.api.getZak(nd)
                            .subscribe(function (res) {
                            _this.zak = res;
                            _this.show = true;
                            loading.dismiss();
                        }, function (err) {
                            console.log(err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    //Переход к просмотру редакции
    ZakinfoPage.prototype.onClick = function (nd, rd) {
        var navigationExtras = {
            queryParams: {
                nd: nd,
                rd: rd
            }
        };
        this.navCtrl.navigateForward(['zakrd'], navigationExtras);
    };
    //Переход к diff
    ZakinfoPage.prototype.onDiff = function () {
        var navigationExtras = {
            queryParams: {
                nd: this.nd,
                f: this.f,
                s: this.s
            }
        };
        this.navCtrl.navigateForward(['diff'], navigationExtras);
    };
    //Переход к просмотру связей
    ZakinfoPage.prototype.onConnections = function (nd) {
        var navigationExtras = {
            queryParams: {
                nd: nd,
            }
        };
        this.navCtrl.navigateForward(['zakconnections'], navigationExtras);
    };
    ZakinfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-zakinfo',
            template: __webpack_require__(/*! ./zakinfo.page.html */ "./src/app/zakinfo/zakinfo.page.html"),
            styles: [__webpack_require__(/*! ./zakinfo.page.scss */ "./src/app/zakinfo/zakinfo.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_gov_base_service__WEBPACK_IMPORTED_MODULE_3__["GovBaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], ZakinfoPage);
    return ZakinfoPage;
}());



/***/ })

}]);
//# sourceMappingURL=zakinfo-zakinfo-module.js.map