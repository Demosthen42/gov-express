(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["zakconnections-zakconnections-module"],{

/***/ "./src/app/zakconnections/zakconnections.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/zakconnections/zakconnections.module.ts ***!
  \*********************************************************/
/*! exports provided: ZakconnectionsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZakconnectionsPageModule", function() { return ZakconnectionsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _zakconnections_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./zakconnections.page */ "./src/app/zakconnections/zakconnections.page.ts");







var routes = [
    {
        path: '',
        component: _zakconnections_page__WEBPACK_IMPORTED_MODULE_6__["ZakconnectionsPage"]
    }
];
var ZakconnectionsPageModule = /** @class */ (function () {
    function ZakconnectionsPageModule() {
    }
    ZakconnectionsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_zakconnections_page__WEBPACK_IMPORTED_MODULE_6__["ZakconnectionsPage"]]
        })
    ], ZakconnectionsPageModule);
    return ZakconnectionsPageModule;
}());



/***/ }),

/***/ "./src/app/zakconnections/zakconnections.page.html":
/*!*********************************************************!*\
  !*** ./src/app/zakconnections/zakconnections.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button (click)=\"onBack(nd)\" defaultHref=\"/zakinfo\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Информация о связях</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content *ngIf=\"show\">\n  <ion-grid no-padding>\n    <ion-row>\n      <ion-col size=\"12\" size-sm>\n        <ion-list-header>\n          <ion-label>\n            <h2>Внешние связи</h2>\n          </ion-label>\n        </ion-list-header>\n        <ion-list>\n          <ion-list>\n            <ion-list-header>\n              <ion-label>См. также</ion-label>\n            </ion-list-header>\n            <ion-item *ngFor=\"let item of info.ref.con_more\" lines=\"full\">\n              <ion-label text-wrap>\n                <h3>{{item.zak}}</h3>\n                <p>{{item.name}}</p>\n                <p>\n                  <ion-button (click)=\"postNewZak(item.nd)\">\n                    Добавить закон\n                  </ion-button>\n                  <ion-button (click)=\"onPreview(item.nd)\">\n                    Предпросмотр\n                  </ion-button>\n                </p>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n          <ion-list>\n            <ion-list-header>\n              <ion-label>Измененяет</ion-label>\n            </ion-list-header>\n            <ion-item *ngFor=\"let item of info.ref.con_changeby\" lines=\"full\">\n              <ion-label text-wrap>\n                <h3>{{item.zak}}</h3>\n                <p>{{item.name}}</p>\n                <p>\n                  <ion-button (click)=\"postNewZak(item.nd)\">\n                    Добавить закон\n                  </ion-button>\n                  <ion-button (click)=\"onPreview(item.nd)\">\n                    Предпросмотр\n                  </ion-button>\n                </p>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n          <ion-list>\n            <ion-list-header>\n              <ion-label>Изменен</ion-label>\n            </ion-list-header>\n            <ion-item *ngFor=\"let item of info.ref.con_change\" lines=\"full\">\n              <ion-label text-wrap>\n                <h3>{{item.zak}}</h3>\n                <p>{{item.name}}</p>\n                <p>\n                  <ion-button (click)=\"postNewZak(item.nd)\">\n                    Добавить закон\n                  </ion-button>\n                  <ion-button (click)=\"onPreview(item.nd)\">\n                    Предпросмотр\n                  </ion-button>\n                </p>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n          <ion-list>\n            <ion-list-header>\n              <ion-label>Отменен</ion-label>\n            </ion-list-header>\n            <ion-item *ngFor=\"let item of info.ref.con_delete\" lines=\"full\">\n              <ion-label text-wrap>\n                <h3>{{item.zak}}</h3>\n                <p>{{item.name}}</p>\n                <p>\n                  <ion-button (click)=\"postNewZak(item.nd)\">\n                    Добавить закон\n                  </ion-button>\n                  <ion-button (click)=\"onPreview(item.nd)\">\n                    Предпросмотр\n                  </ion-button>\n                </p>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n        </ion-list>\n      </ion-col>\n      <ion-col size=\"12\" size-sm>\n        <ion-list-header>\n          <ion-label>\n            <h2>Ссылаются</h2>\n          </ion-label>\n        </ion-list-header>\n        <ion-list>\n          <ion-list>\n            <ion-list-header>\n              <ion-label>См. также</ion-label>\n            </ion-list-header>\n            <ion-item *ngFor=\"let item of info.rvref.ext_more\" lines=\"full\">\n              <ion-label text-wrap>\n                <h3>{{item.zak}}</h3>\n                <p>{{item.name}}</p>\n                <p>\n                  <ion-button (click)=\"postNewZak(item.nd)\">\n                    Добавить закон\n                  </ion-button>\n                  <ion-button (click)=\"onPreview(item.nd)\">\n                    Предпросмотр\n                  </ion-button>\n                </p>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n          <ion-list>\n            <ion-list-header>\n              <ion-label>Измененяет</ion-label>\n            </ion-list-header>\n            <ion-item *ngFor=\"let item of info.rvref.ext_changeby\" lines=\"full\">\n              <ion-label text-wrap>\n                <h3>{{item.zak}}</h3>\n                <p>{{item.name}}</p>\n                <p>\n                  <ion-button (click)=\"postNewZak(item.nd)\">\n                    Добавить закон\n                  </ion-button>\n                  <ion-button (click)=\"onPreview(item.nd)\">\n                    Предпросмотр\n                  </ion-button>\n                </p>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n          <ion-list>\n            <ion-list-header>\n              <ion-label>Изменен</ion-label>\n            </ion-list-header>\n            <ion-item *ngFor=\"let item of info.rvref.ext_change\" lines=\"full\">\n              <ion-label text-wrap>\n                <h3>{{item.zak}}</h3>\n                <p>{{item.name}}</p>\n                <p>\n                  <ion-button (click)=\"postNewZak(item.nd)\">\n                    Добавить закон\n                  </ion-button>\n                  <ion-button (click)=\"onPreview(item.nd)\">\n                    Предпросмотр\n                  </ion-button>\n                </p>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n          <ion-list>\n            <ion-list-header>\n              <ion-label>Отменен</ion-label>\n            </ion-list-header>\n            <ion-item *ngFor=\"let item of info.rvref.ext_delete\" lines=\"full\">\n              <ion-label text-wrap>\n                <h3>{{item.zak}}</h3>\n                <p>{{item.name}}</p>\n                <p>\n                  <ion-button (click)=\"postNewZak(item.nd)\">\n                    Добавить закон\n                  </ion-button>\n                  <ion-button (click)=\"onPreview(item.nd)\">\n                    Предпросмотр\n                  </ion-button>\n                </p>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n        </ion-list>\n      </ion-col>\n\n    </ion-row>\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/zakconnections/zakconnections.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/zakconnections/zakconnections.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3pha2Nvbm5lY3Rpb25zL3pha2Nvbm5lY3Rpb25zLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/zakconnections/zakconnections.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/zakconnections/zakconnections.page.ts ***!
  \*******************************************************/
/*! exports provided: ZakconnectionsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ZakconnectionsPage", function() { return ZakconnectionsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_gov_base_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/gov-base.service */ "./src/app/services/gov-base.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var ZakconnectionsPage = /** @class */ (function () {
    function ZakconnectionsPage(api, loadingController, navCtrl, alertController, route) {
        var _this = this;
        this.api = api;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.route = route;
        this.nd = '';
        this.show = false;
        if (this.show == false) {
            this.route.queryParams.subscribe(function (params) {
                _this.nd = params.nd;
            });
        }
    }
    ZakconnectionsPage.prototype.ionViewDidEnter = function () {
        if (this.show == false) {
            this.getZakInfo(this.nd);
        }
    };
    ZakconnectionsPage.prototype.getZakInfo = function (nd) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'Загрузка информации о ссылках на закон'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.api.getZakInfo(nd)
                            .subscribe(function (res) {
                            _this.info = res;
                            _this.show = true;
                            loading.dismiss();
                        }, function (err) {
                            console.log(err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ZakconnectionsPage.prototype.onBack = function (nd) {
        var navigationExtras = {
            queryParams: {
                nd: nd
            }
        };
        this.navCtrl.navigateBack(['zakinfo'], navigationExtras);
    };
    ZakconnectionsPage.prototype.postNewZak = function (nd) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'Добавление закона'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.api.postNewZak(nd)
                            .subscribe(function (res) {
                            console.log(res);
                            _this.ans = res.ans;
                            _this.presentAlert(_this.ans);
                            loading.dismiss();
                        }, function (err) {
                            console.log(err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ZakconnectionsPage.prototype.presentAlert = function (ans) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Закон добавлен!',
                            message: ans.toString(),
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ZakconnectionsPage.prototype.onPreview = function (nd) {
        var navigationExtras = {
            queryParams: {
                nd: nd,
            }
        };
        this.navCtrl.navigateForward(['preview'], navigationExtras);
    };
    ZakconnectionsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-zakconnections',
            template: __webpack_require__(/*! ./zakconnections.page.html */ "./src/app/zakconnections/zakconnections.page.html"),
            styles: [__webpack_require__(/*! ./zakconnections.page.scss */ "./src/app/zakconnections/zakconnections.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_gov_base_service__WEBPACK_IMPORTED_MODULE_3__["GovBaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], ZakconnectionsPage);
    return ZakconnectionsPage;
}());



/***/ })

}]);
//# sourceMappingURL=zakconnections-zakconnections-module.js.map