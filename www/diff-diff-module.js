(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["diff-diff-module"],{

/***/ "./src/app/diff/diff.module.ts":
/*!*************************************!*\
  !*** ./src/app/diff/diff.module.ts ***!
  \*************************************/
/*! exports provided: DiffPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiffPageModule", function() { return DiffPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _diff_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./diff.page */ "./src/app/diff/diff.page.ts");







var routes = [
    {
        path: '',
        component: _diff_page__WEBPACK_IMPORTED_MODULE_6__["DiffPage"]
    }
];
var DiffPageModule = /** @class */ (function () {
    function DiffPageModule() {
    }
    DiffPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_diff_page__WEBPACK_IMPORTED_MODULE_6__["DiffPage"]]
        })
    ], DiffPageModule);
    return DiffPageModule;
}());



/***/ }),

/***/ "./src/app/diff/diff.page.html":
/*!*************************************!*\
  !*** ./src/app/diff/diff.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button (click)=\"onBack(nd)\" defaultHref=\"/zakinfo\"></ion-back-button>\n        </ion-buttons>\n    <ion-title>Сравнение редакций</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n\n  <ion-list *ngIf=\"show\">\n      <ion-text color=\"primary\" (click)=\"toggle = !toggle\">\n          <h1>Список изменений</h1>\n        </ion-text>\n        <ion-list *ngFor=\"let item of data.diff_mas\" lines=\"full\" [hidden]=\"!toggle\"> \n            <ion-item >\n              <ion-label>{{item.stat}}</ion-label>\n            </ion-item>\n            <ion-text *ngFor=\"let elem of item.diff\" lines=\"full\"> \n                <ion-text *ngIf=\"elem[0]==0\" >\n                  {{elem[1]}}\n                </ion-text>\n                <ion-text *ngIf=\"elem[0]==-1\" color=\"danger\">\n                  <s>{{elem[1]}}</s>  \n                </ion-text>\n                <ion-text *ngIf=\"elem[0]==1\" color=\"success\">\n                    {{elem[1]}}\n                </ion-text>\n            </ion-text>\n        </ion-list>\n        <ion-text color=\"primary\" (click)=\"toggle2 = !toggle2\">\n            <h1>Добавленные пункты и статьи</h1>\n        </ion-text>\n        <ion-list *ngFor=\"let item of data.new_stat\" lines=\"full\" [hidden]=\"!toggle2\"> \n            <ion-item>\n              <ion-label>{{item.stat}}</ion-label>\n            </ion-item>\n            <ion-text>\n                {{item.text}}\n            </ion-text>\n        </ion-list>\n  </ion-list>\n\n\n  \n</ion-content>\n"

/***/ }),

/***/ "./src/app/diff/diff.page.scss":
/*!*************************************!*\
  !*** ./src/app/diff/diff.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RpZmYvZGlmZi5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/diff/diff.page.ts":
/*!***********************************!*\
  !*** ./src/app/diff/diff.page.ts ***!
  \***********************************/
/*! exports provided: DiffPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DiffPage", function() { return DiffPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_gov_base_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/gov-base.service */ "./src/app/services/gov-base.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var DiffPage = /** @class */ (function () {
    function DiffPage(api, loadingController, navCtrl, route) {
        var _this = this;
        this.api = api;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.route = route;
        this.show = false;
        this.route.queryParams.subscribe(function (params) {
            _this.nd = params.nd;
            _this.f = params.f;
            _this.s = params.s;
        });
    }
    DiffPage.prototype.ionViewDidEnter = function () {
        this.getDiff();
    };
    DiffPage.prototype.getDiff = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'Загрузка списка изменений'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        this.api.getDiff(this.nd, this.f, this.s)
                            //this.api.getDiff('102045166', '0', '1')
                            .subscribe(function (res) {
                            _this.data = res;
                            _this.show = true;
                            loading.dismiss();
                        }, function (err) {
                            console.log(err);
                            loading.dismiss();
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DiffPage.prototype.onBack = function (nd) {
        var navigationExtras = {
            queryParams: {
                nd: nd
            }
        };
        this.navCtrl.navigateBack(['zakinfo'], navigationExtras);
    };
    DiffPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-diff',
            template: __webpack_require__(/*! ./diff.page.html */ "./src/app/diff/diff.page.html"),
            styles: [__webpack_require__(/*! ./diff.page.scss */ "./src/app/diff/diff.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_gov_base_service__WEBPACK_IMPORTED_MODULE_3__["GovBaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], DiffPage);
    return DiffPage;
}());



/***/ })

}]);
//# sourceMappingURL=diff-diff-module.js.map