const express = require('express');
const router = express.Router();
const cheerio = require('cheerio');
const request = require('request');
const iconv = require('iconv-lite');
const util = require('util');

const CachemanFile = require('cacheman-file');
const cache = new CachemanFile('cache');

//diff
const diff = require('diff-match-patch-node');
const dmp = diff();
//new diff
var Diff = require('diff');
//Model
const Zak = require('../../models/zakSchema');

const api_key = '89c51112f9b95b6307cb94491fe28cbeb6f5e334';
const app_key = 'appfa8581cf80bb0162493d0048d31bec9a1fae73fe';

//Get List
router.get('/listzak', (req, res) => {
	Zak.getZakList((err, zakList) => {
		if(err){
			throw err;
		}
		res.json(zakList);
	});
});

//Get zak ny nd
router.get('/listzak/:nd', (req, res) => {
    Zak.getZakByNd(req.params.nd, (err, zak) =>{
        if(err){
            throw err;
        }
        var rd_list = [];
        rd_list = zak.rd_list;
        rd_list.sort(function(a, b){
            return a.rd - b.rd;
        });
        res.json({name: zak.name, nd: zak.nd, rds: zak.rds, rd_list});
    });
});

router.get('/listzak/:nd/:rd', (req, res) => {
    Zak.getZakByRd(req.params, (err, zak) =>{
        if(err){
            throw err;
        }
        res.json(zak);
    });
});

//Add Zak
router.post('/listzak', (req, res) => {
	var zak = req.body;
	Zak.addZak(zak, (err, zak) => {
		if(err){
			throw err;
		}
		res.json(zak);
	});
});
//Add Red to existing zak
router.post('/add_red', (req, res) => {
    var red = req.body;
    Zak.addRed(red, (err, red) => {
        if(err){
            throw err;
        }
        res.json(red);
    });
});

//old diff
router.get('/diff2', (req, res) => {
    var f = req.query.f;
    var s = req.query.s;
    articles_f = [];
    articles_s = [];
    match = [];
    diff_mas = [];
    //Обработка первого
    Zak.getZakByRd({nd: req.query.nd, rd:f}, (err, zak1) =>{
        if(err){
            throw err;
        }
        var text = zak1.rd_list[0].text;
        //Подготовка текста
        text = text.replace(/\n/g, ' ');
        text = text.replace(/  /g," ");
        text = text.replace(/ {1,}/g," ");
        text = text.replace(/ /g,"");
        text = text.replace(/С т а т ь я/g,"Статья");
        match = text.match(/ Статья \d{1,}....../g);
        if (match != null) {            
            for(var i = 0; i<match.length; i++) {
                tmp = match[i];
                tmp = tmp.slice(1, tmp.lastIndexOf(' '));
                articles_f.push({stat : tmp, text: text.slice(text.indexOf(match[i]),text.indexOf(match[i+1]))});
            }
        } else {
            articles_f[0] = {stat: 'Текст',text: text};
        }
    //Обработка второго
        Zak.getZakByRd({nd: req.query.nd, rd:s}, (err, zak1) =>{
            if(err){
                throw err;
            }
            var text = zak1.rd_list[0].text;
            //Подготовка текста
            text = text.replace(/\n/g, ' ');
            text = text.replace(/  /g," ");
            text = text.replace(/ {1,}/g," ");
            text = text.replace(/ /g,"");
            text = text.replace(/С т а т ь я/g,"Статья");
            match = text.match(/ Статья \d{1,}....../g);
            if (match != null) {
                for(var i = 0; i<match.length; i++) {
                    tmp = match[i];
                    tmp = tmp.slice(1, tmp.lastIndexOf(' '));
                    articles_s.push({stat : tmp, text: text.slice(text.indexOf(match[i]),text.indexOf(match[i+1]))});
                }
            } else {
                articles_s[0] = {stat: 'Текст',text: text};
            }
        //Сравнение 1
        for (let i=0; i<articles_f.length; i++){
            for (let j=0; j<articles_s.length; j++){
                if (articles_f[i].stat == articles_s[j].stat) {
                    mas = dmp.diff_main(articles_f[i].text,articles_s[j].text);
                    dmp.diff_cleanupSemantic(mas);
                    if(mas.length!=1)
                    diff_mas.push({stat: articles_f[i].stat,diff :mas});
                } 
            }
        }
        //Сравнение 2
        stat_f = articles_f.map((elem)=>{
            return elem.stat;
        });
        stat_s = articles_s.map((elem)=>{
            return elem.stat;
        });
        new_stat =[];
        for (let i=0; i<stat_s.length; i++){
            if (stat_f.indexOf(stat_s[i])==-1){
                for (let j=0; j<articles_s.length; j++){
                    if (articles_s[j].stat==stat_s[i]){
                        new_stat.push({stat: stat_s[i], text: articles_s[j].text})
                    }
                }
            }
        }
        res.json({diff_mas:diff_mas, new_stat: new_stat});
        }); 
    }); 
});

//new diff
router.get('/diff', (req, res) => {
    var f = req.query.f;
    var s = req.query.s;
    articles_f = [];
    articles_s = [];
    match = [];
    diff_mas = [];
    //Обработка первого
    Zak.getZakByRd({nd: req.query.nd, rd:f}, (err, zak1) =>{
        if(err){
            throw err;
        }
        var text = zak1.rd_list[0].text;
        //Подготовка текста
        text = text.replace(/\n/g, ' ');
        text = text.replace(/  /g," ");
        text = text.replace(/ {1,}/g," ");
        text = text.replace(/ /g,"");
        text = text.replace(/С т а т ь я/g,"Статья");
        match = text.match(/ Статья \d{1,}....../g);
        if (match != null) {            
            for(var i = 0; i<match.length; i++) {
                tmp = match[i];
                tmp = tmp.slice(1, tmp.lastIndexOf(' '));
                articles_f.push({stat : tmp, text: text.slice(text.indexOf(match[i]),text.indexOf(match[i+1]))});
            }
        } else {
            articles_f[0] = {stat: 'Текст',text: text};
        }
    //Обработка второго
        Zak.getZakByRd({nd: req.query.nd, rd:s}, (err, zak1) =>{
            if(err){
                throw err;
            }
            var text = zak1.rd_list[0].text;
            //Подготовка текста
            text = text.replace(/\n/g, ' ');
            text = text.replace(/  /g," ");
            text = text.replace(/ {1,}/g," ");
            text = text.replace(/ /g,"");
            text = text.replace(/С т а т ь я/g,"Статья");
            match = text.match(/ Статья \d{1,}....../g);
            if (match != null) {
                for(var i = 0; i<match.length; i++) {
                    tmp = match[i];
                    tmp = tmp.slice(1, tmp.lastIndexOf(' '));
                    articles_s.push({stat : tmp, text: text.slice(text.indexOf(match[i]),text.indexOf(match[i+1]))});
                }
            } else {
                articles_s[0] = {stat: 'Текст', text: text};
            }
        //Сравнение 1
        if (match != null) {
            for (let i=0; i<articles_f.length; i++){
                for (let j=0; j<articles_s.length; j++){
                    if (articles_f[i].stat == articles_s[j].stat) {
                        //mas = dmp.diff_main(articles_f[i].text,articles_s[j].text);
                        //dmp.diff_cleanupSemantic(mas);
                        // if(mas.length!=1)
                        // diff_mas.push({stat: articles_f[i].stat,diff :mas});
                        // var mas = Diff.diffSentences(articles_f[i].text, articles_s[j].text);
                        var mas = Diff.diffWordsWithSpace(articles_f[i].text, articles_s[j].text);
                        //console.log(mas);
                        if(mas.length!=1){
                            var mas1 = [];
                            for (let k=0; k<mas.length; k++){
                                r = mas[k].added ? '1' :
                                    mas[k].removed ? '-1' : '0';
                                mas1.push([r, mas[k].value]);
                            }
                            diff_mas.push({stat: articles_f[i].stat,diff :mas1});
                        }
                        
                    } 
                }
            }
        } else {
            var mas = Diff.diffSentences(articles_f[0].text, articles_s[0].text);
            var mas1 = [];
            for (let k=0; k<mas.length; k++){
                r = mas[k].added ? '1' :
                    mas[k].removed ? '-1' : '0';
                mas1.push([r, mas[k].value]);
            }
            diff_mas.push({stat: articles_f[0].stat,diff :mas1});
        }

        
        //Сравнение 2
        stat_f = articles_f.map((elem)=>{
            return elem.stat;
        });
        stat_s = articles_s.map((elem)=>{
            return elem.stat;
        });
        new_stat =[];
        for (let i=0; i<stat_s.length; i++){
            if (stat_f.indexOf(stat_s[i])==-1){
                for (let j=0; j<articles_s.length; j++){
                    if (articles_s[j].stat==stat_s[i]){
                        new_stat.push({stat: stat_s[i], text: articles_s[j].text})
                    }
                }
            }
        }
        res.json({diff_mas:diff_mas, new_stat: new_stat});
        }); 
    }); 
});

//добавление закона в базу
router.post('/add_zak', (req, res) => {
    request({uri: 'http://pravo.gov.ru/proxy/ips/?docbody=&link_id=0&nd='+req.body.nd+'&bpa=cd00000&bpas=cd00000&firstDoc=1',
    method: 'GET',
    encoding: 'latin1'}, (err, response, html)=>{
        if(!err && response.statusCode == 200) {
            //Получаем имя документа
            var name1 = new String;
            name1 = html.slice(html.indexOf('<title>',1)+7,html.indexOf('</title>', 1));
            name1 = iconv.decode(iconv.encode(name1, 'latin1'),'win1251');
            //Получаем список редакций
            var s = new String;
            s = html.slice(html.indexOf('<select name="doc_editions"',1),html.indexOf('</select>')+9, html.indexOf('<select name="doc_editions"',1));
            const $ = cheerio.load(s);
            data = [];
            $('select').children().each((i, element) => {
                //Получаем дату изменения
                var date = ($(element).text());
                date = iconv.decode(iconv.encode(date, 'latin1'),'win1251');
                //Поулчаем номер редакции
                var rd = ($(element).attr('value'));
                rd = rd.slice(0, rd.indexOf(',',0));
                //Список редакций
                if (rd==0) {var tmp = {date: date, rd: i}}
                    else var tmp = {date: date, rd: rd};
                data.push(tmp);
            });
        }
        Zak.addZak({name: name1, nd: req.body.nd, rds: data.length}, (err, zak) => {
            if(err){
                throw err;
            }
        for (const elem of data){           
                request({uri: 'http://pravo.gov.ru/proxy/ips/?savetext=&nd='+req.body.nd+'&rdk='+elem.rd+'&page=all',
                    method: 'GET',
                    encoding: 'latin1'}, (err, response, html)=>{
                        if(!err && response.statusCode == 200) {
                        var text = new String;
                        text = html;               
                        text = iconv.decode(iconv.encode(text, 'latin1'),'win1251');
                        if (text =='') text = 'Редакция не готова';
                        Zak.addRed({nd: req.body.nd, rd: elem.rd, date: elem.date, text: text}, (err, zak) => {
                            if(err){
                            throw err;
                            }
                            });
                        }
                });
            } 
        });    
       res.json({ans: 'Закон '+name1+' добавлен. '+data.length+' редакций добавлено'}); 
    })
});

router.post('/find', (req, res) => {
    //Генерерация таблицы win1251->URL-encode
    var decodeMap = [];
    let win1251 = new util.TextDecoder('cp1251');
    for (var i = 0x00; i <= 0xFF; i++) {
        var hex = (i <= 0x0F ? "0" : "")+i.toString(16).toUpperCase();
        decodeMap[win1251.decode(Uint8Array.from([i]))] = '%'+hex;
        }
    decodeMap[' '] = '+';
    decodeMap['-'] = '-';
    for (var i = 0; i <= 9; i++){
        decodeMap[i] = i;
    }
   //win1251->URL-encode
    function Win1251ToDOMString(str) {
        var res = '';
        for (var i = 0; i < str.length; i++){
            res += decodeMap[str[i]];
        }
        return res;
    }
    //Запрос
    st = req.body.st;
    st = Win1251ToDOMString(st);
    //res.json(decodeMap);
    url = 'http://pravo.gov.ru/proxy/ips/?list_itself=&bpas=cd00000%2Fr045900&intelsearch=+'+st+'&sort=-1&page=first';
    request({uri: url,
    method: 'GET',
    encoding: 'latin1'}, (err, response, html)=>{
        if(!err && response.statusCode == 200) {
            var text = html;
            text = iconv.decode(iconv.encode(text, 'latin1'),'win1251');
            ans = [];
            match = text.split("<!-- BEGIN элемент списка -->");
            for(var i = 1; i<match.length; i++){                
                match[i].slice(text.indexOf('<table class="l_name">',1),text.indexOf('</table>')+8, text.indexOf('<table class="l_name">',1));
                var $ = cheerio.load(match[i]);
                var zak = $('a').text().trim();
                zak = zak.replace('  ', ' ');
                var name = $('span').text();
                name = name.slice(name.lastIndexOf("\n\t\t\t\t")+5, name.length);
                var nd = $('a').attr('href');
                nd = nd.slice(nd.indexOf('nd=')+3, nd.indexOf('&', nd.indexOf('nd=')));
                ans.push({zak: zak, name: name, nd: nd});
            }            
            res.send(ans);
        } else console.log(err);
    });
});

router.get('/info', (req, res) => {
    var url = 'http://pravo.gov.ru/proxy/ips/?docrefs.xml=&oid='+req.query.nd;
    const key = req.url;
    // cache.get(key, (err, val) => {
    //     if (err == null && val != null) {
    //         res.json(val);
    //     } else {
            request({uri: url,
                method: 'GET',
                encoding: 'latin1'}, (err, response, html)=>{
                    //Внешние связи
                    var text = html;
                    text = iconv.decode(iconv.encode(text, 'latin1'),'win1251');
                    ext_more=[];
                    ext_changeby=[];
                    ext_change=[];
                    ext_delete=[];
                    const tmp = cheerio.load(text);
                    tmp('docrefslist').children('reference').each((i, elem) => {
                        const $ = cheerio.load(elem);
                        dop = $(elem).children('refkind').text();
                        name = $(elem).children('docannot').text();
                        zak = $(elem).children('docname').text();
                        nd = $(elem).children('docname').attr('oid');
                        if (dop=="См. также") ext_more.push({dop:dop, name: name, zak: zak, nd: nd});
                        if (dop=="Изменен") ext_changeby.push({dop:dop,name: name, zak: zak, nd: nd});
                        if (dop=="Изменяет") ext_change.push({dop:dop,name: name, zak: zak, nd: nd});
                        if (dop=="Отменяет") ext_delete.push({dop:dop,name: name, zak: zak, nd: nd});                      
                    });
        
                    request({uri: 'http://pravo.gov.ru/proxy/ips/?docrefs.xml=&refs=1&oid='+req.query.nd,
                        method: 'GET',
                        encoding: 'latin1'}, (err, response, html)=>{
                            var text = html;
                            text = iconv.decode(iconv.encode(text, 'latin1'),'win1251');
                            con_more=[];
                            con_changeby=[];
                            con_change=[];
                            con_delete=[];
                            const tmp = cheerio.load(text);
                            tmp('docrefslist').children('reference').each((i, elem) => {
                                const $ = cheerio.load(elem);
                                dop = $(elem).children('refkind').text();
                                name = $(elem).children('docannot').text();
                                zak = $(elem).children('docname').text();
                                nd = $(elem).children('docname').attr('oid');
                                if (dop=="См. также") con_more.push({dop:dop,name: name, zak: zak, nd: nd});
                                if (dop=="Изменен") con_changeby.push({dop:dop,name: name, zak: zak, nd: nd});
                                if (dop=="Изменяет") con_change.push({dop:dop,name: name, zak: zak, nd: nd});
                                if (dop=="Отменяет") con_delete.push({dop:dop,name: name, zak: zak, nd: nd});                       
                            });
                            res.json({ref: {con_more, con_changeby, con_change, con_delete}, rvref: {ext_more,ext_changeby, ext_change,ext_delete}});
                            // cache.set(key, {ref: {con_more, con_changeby, con_change, con_delete}, rvref: {ext_more,ext_changeby, ext_change,ext_delete}}, "1d");
                        });    
                });
        // }
    // });

    
});


router.get('/preview/:nd', (req, res)=>{
    const key = req.url;
    cache.get(key, (err, val) => {
        if (err == null && val != null) {
           res.json(val);
        } else {
            request({uri: 'http://pravo.gov.ru/proxy/ips/?savetext=&nd='+req.params.nd,
            method: 'GET',
            encoding: 'latin1'}, (err, response, html)=>{
                if(!err && response.statusCode == 200) {
                var text = new String;
                text = html;                  
                text = iconv.decode(iconv.encode(text, 'latin1'),'win1251');
                if (text =='') text = 'Редакция не готова';
                res.json(text);
                // cache.set(key, text, "1d");
                }
            });
        }
        
    });

})

module.exports = router;