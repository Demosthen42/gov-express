# GOV-express

Wrapper for pravo.gov
Student project
More information [here](https://disk.yandex.ru/d/A9JAI304-WF6pQ) and [here](https://na-journal.ru/2-2019-tehnicheskie-nauki/1632-informacionno-analiticheskaya-sistema-poiska-normativnogo-obespecheniya-socialnoi-podderzhki-naseleniya-rf)
Test [here](https://pure-depths-85523.herokuapp.com/home)
### Installing

To init project

```bash
cd gov-express

npm install
```

Create ```.env``` config file according to ```.env.example```

### Run

To run project

```bash
npm run dev
```
