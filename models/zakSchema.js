const mongoose = require('mongoose');

const rdSchema = mongoose.Schema({
    rd: {
        type: Number,
        required: true
    },
    date: {
        type: String
    },
    text:{
        type: String
    }
});

const zakSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    nd: {
        type: Number,
        required: true
    },
    rds: {
        type: Number,
        required: true
    },
    rd_list: [rdSchema]
});

const Zak = module.exports = mongoose.model('zak_list', zakSchema);

//Get Zak
module.exports.getZakList = (callback, limit) => {
	Zak.find({},{"_id":0, "name":1, "nd":1, "rds": 1}, callback);
}

//Get Zak by id
module.exports.getZakById = (id, callback) => {
    Zak.findById(id,{"_id":0, "name":1, "nd":1, "rds": 1, }, callback);
}

//Get Zak by nd
module.exports.getZakByNd = (nd, callback) =>{
    Zak.findOne({nd: nd},{"_id":0, "rd_list.text":0,"rd_list._id":0}, callback);
}

//Get Red by nd, rd
module.exports.getZakByRd = (req, callback) =>{
    Zak.findOne({'nd': req.nd, 'rd_list.rd': req.rd},{'rd_list.$':1}, callback)
}

//Add Zak
module.exports.addZak = (zak, callback) => {
	Zak.create(zak, callback);
}

//Add Red
module.exports.addRed = (req, callback) => {
    var red = {rd: req.rd, date: req.date, text: req.text};
    Zak.findOneAndUpdate({nd: req.nd}, {$push: {rd_list: red}}, {new: true},callback);
}